package org.smart4j.chapter1;
import org.apache.commons.lang3.ArrayUtils;
import org.smart4j.chapter1.bean.Data;
import org.smart4j.chapter1.bean.Handler;
import org.smart4j.chapter1.bean.Param;
import org.smart4j.chapter1.bean.View;
import org.smart4j.chapter1.helper.BeanHelper;
import org.smart4j.chapter1.helper.ControllerHelper;
import org.smart4j.chapter1.util.CodecUtil;
import org.smart4j.chapter1.util.JsonUtil;
import org.smart4j.chapter1.util.ReflectionUtil;
import org.smart4j.chapter1.util.StreamUtil;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

@WebServlet(urlPatterns="/*",loadOnStartup=0)
public class DispatcherServlet extends HttpServlet {
    @Override
    public void init(ServletConfig servletConfig)throws ServletException{
        HelperLoader.init();
        //获取ServletContext对象
        ServletContext servletContext=servletConfig.getServletContext();
        //注册处理JSP的Servlet
        ServletRegistration jspServlet=servletContext.getServletRegistration("jsp");
        jspServlet.addMapping("/WEB-INF/jsp/*");
        //注册处理静态资源默认Servlet
        ServletRegistration defaultServlet=servletContext.getServletRegistration("default");
        defaultServlet.addMapping("/html/*");



    }

    @Override
    public void service(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        //请求方法与请求路径
        String requestMothod = request.getMethod().toLowerCase();
        String requestPath = request.getPathInfo();
        //获取Actionc处理器
        Handler handler= ControllerHelper.getHandler(requestMothod,requestPath);
        if (handler != null){
            //获取Controller类及其实例
            Class<?> controllerClass=handler.getControllerClass();
            Object contorllerBean= BeanHelper.getBean(controllerClass);
            //创建请求参数对象
            Map<String,Object> paramMap=new HashMap<String, Object>();
            Enumeration<String> paramNames=request.getParameterNames();
            while(paramNames.hasMoreElements()){
                String paramsName=paramNames.nextElement();
                String paramValue=request.getParameter(paramsName);
                paramMap.put(paramsName,paramValue);
            }
            String boby= CodecUtil.decodeURL(StreamUtil.getString(request.getInputStream()));
            if(!"".equals(boby)){
                String[] params=boby.split("&");
                if(!ArrayUtils.isEmpty(params)){
                    for (String param:params){
                        String[] array=param.split("=");
                        if (!ArrayUtils.isEmpty(array) && array.length==2){
                            String paramName=array[0];
                            String paramValue=array[1];
                            paramMap.put(paramName,paramValue);
                        }
                    }
                }
            }

            Param param=new Param(paramMap);
            //调用Action方法
            Method actionMethod=handler.getActionMethod();
            Object result = ReflectionUtil.invokeMethod(contorllerBean,actionMethod,param);

            //处理Action方法返回值
            if (result instanceof View){
                View view=(View)result;
                String path=view.getPath();
                if (!"".equals(path)){
                    if (path.startsWith("/")){
                        response.sendRedirect(request.getContextPath()+path);
                    }else if("jsp".equals(path.substring(path.length()-3,path.length()))){
                        Map<String,Object> model=view.getModel();
                        for (Map.Entry<String,Object> entry:model.entrySet()){
                            request.setAttribute(entry.getKey(),entry.getValue());
                        }
                        request.getRequestDispatcher("/WEB-INF/jsp/"+path).forward(request,response);
                    }else if("html".equals(path.substring(path.length()-4,path.length()))){

                        request.getRequestDispatcher("/html/"+path).forward(request,response);
                    }else if("js".equals(path.substring(path.length()-2,path.length()))){

                        request.getRequestDispatcher("/html/"+path).forward(request,response);
                    }
                }
            }else if(result instanceof Data){
                Data data=(Data)result;
                Object model=data.getModel();
                if(model !=null){
                    response.setContentType("application/json");
                    response.setCharacterEncoding("UTF-8");
                    PrintWriter writer=response.getWriter();
                    String json= JsonUtil.toJson(model);
                    writer.write(json);
                    writer.flush();
                    writer.close();
                }
            }


        }
    }
}
