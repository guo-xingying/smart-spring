package org.smart4j.chapter1.proxy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Method;

public class AspectProxy implements Proxy {
    private static final Logger log= LoggerFactory.getLogger(AspectProxy.class);


    public Object doProxy(ProxyChain proxyChain) throws Throwable {
        Object resut=null;
        Class<?> cls=proxyChain.getTargetClass();
        Method method=proxyChain.getTargetMethod();
        Object[] params=proxyChain.getMethodParams();

        try{
            if (intercept(cls,method,params)){
                before(cls,method,params);
                resut=proxyChain.doProxyChain();

            }else {
                resut=proxyChain.doProxyChain();
            }
        }catch (Exception e){
            log.error("proxy",e);
        }
        return resut;
    }

    private boolean intercept(Class<?> cls, Method method, Object[] params) {
        return true;
    }

    public void before(Class<?> cls, Method method,Object[] params) throws Throwable{

    }
}
