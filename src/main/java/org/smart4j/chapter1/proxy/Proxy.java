package org.smart4j.chapter1.proxy;

public interface Proxy {
    /**
     * 执行链式代理
     */
    Object doProxy(ProxyChain proxyChain)throws Throwable;
}
