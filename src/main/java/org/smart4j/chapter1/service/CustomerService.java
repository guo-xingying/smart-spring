package org.smart4j.chapter1.service;

import org.smart4j.chapter1.annotation.Service;
import org.smart4j.chapter1.model.Customer;
import org.smart4j.chapter1.util.DatabaseHelper;

import java.util.List;
import java.util.Map;

@Service
public class CustomerService {

    /**
     * 获取客户列表
     */
    public List<Customer> getCustomerList() {

        String sql="select * from customer";
        List list= DatabaseHelper.queryEntityList(Customer.class,sql);
        return list;
    }

    /**
     * 获取客户
     * @param id
     * @return
     */
    public Customer getCustomer(long id){
        String sql="select * from customer where id=?";
        Object[] params=new Object[]{id};
        Customer customer=DatabaseHelper.queryEntity(Customer.class,sql,params);
        return customer;
    }

    /**
     * 创建客户
     * @param filedMap
     * @return
     */
    public boolean createCustomer(Map<String,Object> filedMap){

        return  DatabaseHelper.insertEntity(Customer.class,filedMap);
    }
    /**
     * 更新客户
     */
    public boolean updateCustomer(long id,Map<String,Object> fieldMap){
        return DatabaseHelper.updateEntity(Customer.class,id,fieldMap);
    }

}
