package org.smart4j.chapter1.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Member;
import java.lang.reflect.Method;

/**
 * 反射工具类
 */
public class ReflectionUtil {
    private static final Logger log=LoggerFactory.getLogger(ReflectionUtil.class);

    /**
     * 创建实例
     */
    public static Object newInstance(Class<?> cls){
        Object instance = null;
        try{
            instance=cls.newInstance();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
        return instance;
    }
    /**
     * 调用方法
     */
    public static Object invokeMethod(Object obj, Method method,Object...args){
        Object resilt=null;
        try{
            method.setAccessible(true);
            resilt=method.invoke(obj,args);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return resilt;
    }

    public static void setField(Object beanInstance, Field beanField, Object beanFieldInstance) {
        try{
            beanField.setAccessible(true);
            beanField.set(beanInstance,beanFieldInstance);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * 设置成员变量的值
     */
}
