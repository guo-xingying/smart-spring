package org.smart4j.chapter1.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


public class PropsUtil {
    private static final Logger log= LoggerFactory.getLogger(PropsUtil.class);

    public static Properties loadProps(String fileName){
        Properties properties=null;
        InputStream in=null;
        try{
            in=Thread.currentThread().getContextClassLoader().getResourceAsStream(fileName);
            properties=new Properties();
            properties.load(in);
        }catch(IOException e){
            log.error("load properties failure",e);
        }finally {
            if(in!=null){
                try{
                    in.close();
                }catch (IOException e){
                    log.error("close input fileure",e);
                }
            }
        }
        return properties;
    }
    public static int getInt(Properties properties,String key){
        if(properties.contains(key)){
            return Integer.parseInt(properties.getProperty(key));
        }else{
            return 0;
        }
    }
    public static String getString(Properties properties,String key){
        if(properties.contains(key)){
            return properties.getProperty(key);
        }else{
            return "";
        }
    }
}
