package org.smart4j.chapter1.util;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.*;
import java.util.Date;

public class DatabaseHelper {
    private static final String DRIVER;
    private static final String URL;
    private static final String USER;
    private static final String PASSWORD;

    private static final BasicDataSource DATA_SOURCE;

    private static final Logger log= LoggerFactory.getLogger(DatabaseHelper.class);
    static {
        //CONNECTION_HOLDER=new ThreadLocal<Connection>();
        //QUERY_RUNNER=new QueryRunner();
        Properties properties= PropsUtil.loadProps("config.properties");
        DRIVER=properties.getProperty("jdbc.driver");
        URL=properties.getProperty("jdbc.url");
        USER=properties.getProperty("jdbc.username");
        PASSWORD=properties.getProperty("jdbc.password");

        DATA_SOURCE=new BasicDataSource();
        DATA_SOURCE.setDriverClassName(DRIVER);
        DATA_SOURCE.setUrl(URL);
        DATA_SOURCE.setUsername(USER);
        DATA_SOURCE.setPassword(PASSWORD);

    }
    private static final QueryRunner QUERY_RUNNER=new QueryRunner();
    private static final ThreadLocal<Connection> CONNECTION_HOLDER=new ThreadLocal<Connection>();
    private static long start=System.currentTimeMillis( );
    /**
     * 获取数据库连接
     */
    public static Connection getConnection() throws SQLException {
        Connection conn=CONNECTION_HOLDER.get();
        long timeNew=System.currentTimeMillis();
        long diff=timeNew-start;
        if(conn==null || conn.isClosed()){
            try{
                conn= DATA_SOURCE.getConnection();
                start=timeNew;
            }catch (SQLException e){
                log.error("get connection failure",e);
                throw new RuntimeException(e);
            }finally {
                CONNECTION_HOLDER.set(conn);
            }
        }
        return conn;
    }

    /**
     * 关闭数据库连接
     */
    public static void closeConnection(){
        Connection conn=CONNECTION_HOLDER.get();
        if(conn!=null){
            try{
                conn.close();
            }catch (SQLException e){
                throw  new RuntimeException(e);
            }finally {
                CONNECTION_HOLDER.remove();
            }
        }
    }


    public static <T> List<T> queryEntityList(Class<T> entityClass,String sql,Object... params){
        List<T> entityList = null;
        try{
            Connection conn=getConnection();
            entityList=QUERY_RUNNER.query(conn,sql,new BeanListHandler<T>(entityClass),params);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            //closeConnection();
        }
        return entityList;
    }

    public static <T> T queryEntity(Class<T> entityClass,String sql,Object... params){
        T entity = null;
        try{
            Connection conn=getConnection();
            entity=QUERY_RUNNER.query(conn,sql,new BeanHandler<T>(entityClass),params);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            //closeConnection();
        }
        return entity;
    }
    /**
     * 执行更新语句（包括update、insert、delete）
     */
    public static int executeUpdate(String sql,Object... params){
        int rows=0;
        try{
            Connection conn=getConnection();
            rows=QUERY_RUNNER.update(conn,sql,params);
        }catch (SQLException e){
            throw new RuntimeException();
        }finally {
            //closeConnection();
        }
        return rows;
    }

    /**
     * 插入实体
     */
    public static <T> boolean insertEntity(Class<T> entityClass, Map<String,Object> filedMap){
        String sql="instert into "+entityClass.getName();
        StringBuffer columns=new StringBuffer("(");
        StringBuffer values=new StringBuffer("(");
        for(String key:filedMap.keySet()){
            columns.append(key).append(",");
            values.append("?,");
        }
        columns.replace(columns.lastIndexOf(","),columns.length(),")");
        values.replace(values.lastIndexOf(","),values.length(),")");
        sql+=columns+"valuse"+values;
        Object[] params=filedMap.values().toArray();
        return executeUpdate(sql,params)==1;

    }

    /**
     * 更新实体
     */
    public static <T> boolean updateEntity(Class<T> entityClass,long id,Map<String,Object> filedMap) {
        if(CollectionUtils.isEmpty((Collection<?>) filedMap)){
            log.error("can not update entity:fileMap is empty");
            return false;
        }
        String sql="update "+entityClass.getName()+" set ";
        StringBuffer columns=new StringBuffer();
        for(String key:filedMap.keySet()){
            columns.append(key).append("=?, ");
        }
        sql+=columns.substring(0,columns.lastIndexOf(", "))+" where id=?";
        List<Object> paramList =new ArrayList<Object>();
        paramList.addAll(filedMap.values());
        paramList.add(id);
        Object[] params=paramList.toArray();
        return executeUpdate(sql,params)==1;
    }
}
