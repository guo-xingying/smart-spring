package org.smart4j.chapter1.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonUtil {
    private static final ObjectMapper OBJECT_MAPPER=new ObjectMapper();

    public static <T>String toJson(T obj){
        String json = null;
        try{
            json=OBJECT_MAPPER.writeValueAsString(obj);
            
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return json;
    }
}
