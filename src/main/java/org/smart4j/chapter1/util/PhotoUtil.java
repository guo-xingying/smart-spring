package org.smart4j.chapter1.util;


import java.awt.Font;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.List;


import javax.imageio.ImageIO;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;

import org.jfree.chart.plot.XYPlot;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.smart4j.chapter1.model.zufang_mast;
import sun.misc.BASE64Encoder;


public class PhotoUtil {


    public static String setChart(List<zufang_mast> list)  throws IOException {
        XYSeries anxuan = new XYSeries("安选");
        XYSeries noAnxuan = new XYSeries("非安选");
        for (zufang_mast mast:list){
            if (mast.getAnxuan()==1){
                anxuan.add(mast.getMoney(),mast.getRoom_area());
            }else {
                noAnxuan.add(mast.getMoney(),mast.getRoom_area());
            }
        }

        //添加到数据集
        XYSeriesCollection dataset = new XYSeriesCollection();
        dataset.addSeries(anxuan);
        dataset.addSeries(noAnxuan);

        //实现简单的散点图，设置基本的数据
        JFreeChart chart = ChartFactory.createScatterPlot(
                list.get(0).getXiaoqu(),// 图表标题
                "价格",//y轴方向数据标签
                "面积",//x轴方向数据标签
                dataset,//数据集，即要显示在图表上的数据
                PlotOrientation.VERTICAL,//设置方向
                true,//是否显示图例
                true,//是否显示提示
                false//是否生成URL连接
        );


        Font font = new Font("宋体", Font.BOLD, 20);
        //设置整个图片的标题字体
        chart.getTitle().setFont(font);
        //设置提示条字体
        font = new Font("宋体", Font.BOLD, 15);
        chart.getLegend().setItemFont(font);

        //得到绘图区
        XYPlot plot = (XYPlot) chart.getPlot();
        //得到绘图区的域轴(横轴),设置标签的字体
        plot.getDomainAxis().setLabelFont(font);
        //设置横轴标签项字体
        plot.getDomainAxis().setTickLabelFont(font);
        //设置范围轴(纵轴)字体
        plot.getRangeAxis().setLabelFont(font);
        plot.setForegroundAlpha(1.0f);




        BufferedImage bufferedImage=chart.createBufferedImage(600,400);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();//io流
        ImageIO.write(bufferedImage, "png", baos);//写入流中
        byte[] bytes = baos.toByteArray();//转换成字节
        baos.close();
        BASE64Encoder encoder = new BASE64Encoder();
        String png_base64 = encoder.encodeBuffer(bytes).trim();//转换成base64串
        png_base64 = png_base64.replaceAll("\n", "").replaceAll("\r", "");//删除 \r\n

        //        ImageIO.write(bufferedImage, "png", new File("D:/qrcode1.png"));
        System.out.println("值为："+"data:image/jpg;base64,"+png_base64);
        return "data:image/jpg;base64,"+png_base64;
    }


}
