package org.smart4j.chapter1.bean;

import net.sf.cglib.asm.$ClassReader;

import java.util.Map;

public class Param {

    private Map<String,Object> paramMap;
    public Param(Map<String,Object> paramMap){
        this.paramMap=paramMap;
    }
    public Map<String,Object> getParamMap(){
        return paramMap;
    }

}
