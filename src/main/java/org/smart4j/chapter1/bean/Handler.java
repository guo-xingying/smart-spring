package org.smart4j.chapter1.bean;

import java.lang.reflect.Method;

public class Handler {
    private Class<?> controlerClass;

    /**
     * Action方法
     */
    private Method actionMethod;

    public Handler(Class<?> controlerClass,Method actionMethod){
        this.controlerClass=controlerClass;
        this.actionMethod=actionMethod;
    }

    public Class<?> getControllerClass() {
        return controlerClass;
    }
    public Method getActionMethod(){
        return actionMethod;
    }
}
