package org.smart4j.chapter1.helper;

import org.apache.commons.collections4.CollectionUtils;
import org.smart4j.chapter1.annotation.Action;
import org.smart4j.chapter1.bean.Handler;
import org.smart4j.chapter1.bean.Request;
import org.smart4j.chapter1.util.ArrayUtil;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class ControllerHelper {
    /**
     * 用于存放请求与处理器的映射关系
     */
    private static final Map<Request, Handler> action_map=new HashMap<Request, Handler>();

    static {
        Set<Class<?>> controllerClassSet=ClassHelper.getControllerClassSet();
        if (CollectionUtils.isNotEmpty(controllerClassSet)){
            for (Class<?> controllerClass:controllerClassSet){
                Method[] methods=controllerClass.getDeclaredMethods();
                if (ArrayUtil.isNotEmpty(methods)){
                    for (Method method:methods){
                        if (method.isAnnotationPresent(Action.class)){
                            Action action=method.getAnnotation(Action.class);
                            String mapping=action.value();

                            String[] array=mapping.split(":");
                            if (ArrayUtil.isNotEmpty(array)&&array.length==2){
                                String requestMethod=array[0];
                                String requestPath=array[1];
                                Request request=new Request(requestMethod,requestPath);
                                Handler handler=new Handler(controllerClass,method);

                                action_map.put(request,handler);
                            }
                        }
                    }
                }
            }
        }
    }
    /**
     * 获取Handler
     */
    public static  Handler getHandler(String requestMothod,String requestPath){
        Request request = new Request(requestMothod,requestPath);
        return action_map.get(request);
    }
}
