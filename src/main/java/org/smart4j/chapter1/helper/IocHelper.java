package org.smart4j.chapter1.helper;



import org.apache.commons.collections4.CollectionUtils;
import org.smart4j.chapter1.annotation.Inject;
import org.smart4j.chapter1.util.ReflectionUtil;
import org.smart4j.chapter1.util.ArrayUtil;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;

import static org.apache.commons.collections4.MapUtils.isEmpty;

public class IocHelper {
    static {
        Map<Class<?>,Object> beanMap=BeanHelper.getBeanMap();
        if (!isEmpty(beanMap)){
            //遍历Bean Map
            for (Map.Entry<Class<?>,Object>beanEntry:beanMap.entrySet()){
                //从
                Class<?> beanClass=beanEntry.getKey();
                Object beanInstance=beanEntry.getValue();
                //获取bean所有成员变量
                Field[] beanFields=beanClass.getDeclaredFields();
                if(ArrayUtil.isNotEmpty(beanFields)){
                    //遍历Bean Field
                    for (Field beanField:beanFields){
                        if (beanField.isAnnotationPresent(Inject.class)){
                            //在  获取实例
                            Class<?> beanFieldClass = beanField.getType();
                            Object beanFieldInstance = beanMap.get(beanFieldClass);
                            if(beanFieldInstance!=null){
                                ReflectionUtil.setField(beanInstance,beanField,beanFieldInstance);
                            }
                        }
                    }
                }
            }
        }
    }
}
