package org.smart4j.chapter1.aspect;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.smart4j.chapter1.annotation.Aspect;
import org.smart4j.chapter1.annotation.Controller;
import org.smart4j.chapter1.proxy.AspectProxy;

import java.lang.reflect.Method;

@Aspect(Controller.class)
public class ControllerAspect extends AspectProxy {
    private static final Logger log= LoggerFactory.getLogger(ControllerAspect.class);

    @Override
    public void before(Class<?> cls, Method method,Object[] params) throws Throwable{
        log.debug("------------begin-----------");
    }
}
