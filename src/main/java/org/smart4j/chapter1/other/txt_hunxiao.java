package org.smart4j.chapter1.other;

import java.io.*;
import java.util.ArrayList;

public class txt_hunxiao {

    public static void main(String[] args) throws IOException {
        txt();
    }

    private static void txt() throws IOException {
        File file=new File("D:\\java\\idea\\smartSpring\\src\\main\\resources\\txt\\001.txt");
        BufferedReader br = new BufferedReader(new FileReader(file));//构造一个BufferedReader类来读取文件
        String s = null;
        int count=0;
        ArrayList<StringBuffer> list=new ArrayList();
        while((s = br.readLine())!=null){//使用readLine方法，一次读一行
            if(s.length()>2 && s.charAt(0)=='第' && (s.charAt(3)=='章' || s.charAt(4)=='章')){

                System.out.println(s);
                for (int j=list.size()-1;j>=0;j--){
                    System.out.println(list.get(j));
                }
            }else{

                char[] ch = s.toCharArray();
                StringBuffer sb = new StringBuffer();
                for (int i = ch.length - 1; i >= 0; i--) {
                    sb.append(ch[i]);
                }
                list.add(sb);
            }
        }
    }
}
