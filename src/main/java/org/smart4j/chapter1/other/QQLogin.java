package org.smart4j.chapter1.other;


import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.AppiumDriver;

public class QQLogin {

    public static void main1(String[] args) throws MalformedURLException, InterruptedException {
        //打包DesiredCapabilities这个类
        DesiredCapabilities des = new DesiredCapabilities();
        des.setCapability("platformName", "Android");
        des.setCapability("platformVersion", "10");
        des.setCapability("deviceName", "MI_9_SE");
        des.setCapability("noReset", true);
        //获取包名及第一个页面的activity
        des.setCapability("appPackage", "com.jingdong.app.mall");
        des.setCapability("appActivity", "MainFrameActivity");
        des.setCapability("newCommandTimeout","180");
        des.setCapability("implicitlyWait","30");

        //创建AppiumDriver对象，连接上Appium服务器，并将相应的键值对传过去
        AppiumDriver driver = null;
        driver = new AppiumDriver(new URL("http://127.0.0.1:4723/wd/hub"),des);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        //步骤1：点击首页的登录按钮
        driver.findElementByAccessibilityId("购物车").click();
        //步骤2：在登录页面用户名文本框中输入qq号码
        driver.findElementByAccessibilityId("请输入QQ号码或手机或邮箱").clear();
        driver.findElementByAccessibilityId("请输入QQ号码或手机或邮箱").sendKeys("2572652583");
        //步骤3：在登录页面密码文本框中输入异常的密码（123456789）
        Thread.sleep(2000);
        driver.findElementById("com.tencent.qqlite:id/password").click();
        driver.findElementById("com.tencent.qqlite:id/password").sendKeys("123456789");
        //步骤4：点击登录按钮
        //driver.findElementByAccessibilityId("登录QQ").click();
        //为了能够正常地跳转到验证码页面，又添加了一个强制线程的等待
        //Thread.sleep(8000);
        //滑动验证码的图标到相应的位置
        //driver.swipe(169, 334, 347, 344, 3000);
        //符合qq的组成规范的号码不一定是qq号码,需要断言一下
        boolean flag = false; //= driver.findElementByAndroidUIAutomator("text('登录失败')").isDisplayed();
        if(flag==true){
            System.out.println("测试用例通过");
        }

    }
}