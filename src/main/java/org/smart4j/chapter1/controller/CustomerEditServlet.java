package org.smart4j.chapter1.controller;


import org.smart4j.chapter1.model.Customer;
import org.smart4j.chapter1.service.CustomerService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

//@WebServlet("/customer")
public class CustomerEditServlet extends HttpServlet {
    private CustomerService customerService;

    @Override
    public void init(){
        customerService=new CustomerService();
    }
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
        int id=Integer.parseInt(req.getParameter("id"));
        Customer customer=customerService.getCustomer(id);
        //List<Customer> customerList=customerService.updateCustomer(Customer,id);
        req.setAttribute("customer",customer);
        req.getRequestDispatcher("/WEB-INF/jsp/customerEdit.jsp").forward(req,resp);
    }

}
