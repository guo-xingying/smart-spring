package org.smart4j.chapter1.controller;

import org.smart4j.chapter1.annotation.Action;
import org.smart4j.chapter1.annotation.Controller;
import org.smart4j.chapter1.annotation.Inject;
import org.smart4j.chapter1.bean.Param;
import org.smart4j.chapter1.bean.View;
import org.smart4j.chapter1.model.Customer;
import org.smart4j.chapter1.model.shushuwu_content;
import org.smart4j.chapter1.service.CustomerService;
import org.smart4j.chapter1.util.DatabaseHelper;

import java.util.List;
    @Controller
    public class shushuwuController {
        /**
         * 进入客户界面
         */
        @Action("get:/shu")
        public View index(Param param){
            String word= (String) param.getParamMap().get("word");

            String sql="SELECT title,chapter01,chapter02_xiu,content_xiu FROM shushuwu_content WHERE MATCH (content_xiu) AGAINST (? IN BOOLEAN MODE)";
            List<shushuwu_content> list = DatabaseHelper.queryEntityList(
                    shushuwu_content.class,sql,word);

            return new View("customer.jsp").addModel("shushuwuList",list);
        }
    /**
     * 目录
     */
    @Action("get:/mulu")
    public View mulu(Param param){
        int chapter01= Integer.parseInt((String) param.getParamMap().get("chapter01"));

        String sql="select chapter02 from shushuwu_content where chapter01 = ? limit 1" ;
        List<shushuwu_content> list = DatabaseHelper.queryEntityList(
                shushuwu_content.class,sql,chapter01);
        int index=list.get(0).getChapter02();
        String sql1="select title,chapter02,chapter02- ? chapter02_xiu,left(GROUP_CONCAT(content_xiu),50) content_xiu from shushuwu_content where chapter01=? group by chapter02";
        List<shushuwu_content> list1 = DatabaseHelper.queryEntityList(
                shushuwu_content.class,sql1,index-1,chapter01);

        return new View("mulu.jsp").addModel("shushuwuList",list1);
    }

    /**
     * 正文
     */
    @Action("get:/content")
    public View content(Param param){
        int chapter02= Integer.parseInt((String) param.getParamMap().get("chapter02"));

        String sql="SELECT GROUP_CONCAT(content_xiu) content_xiu FROM shushuwu_content WHERE chapter02=? group by chapter02" ;
        List<shushuwu_content> list = DatabaseHelper.queryEntityList(
                shushuwu_content.class,sql,chapter02);

        return new View("content.jsp").addModel("shushuwuList",list);
    }


}
