package org.smart4j.chapter1.controller;

import org.smart4j.chapter1.annotation.Action;
import org.smart4j.chapter1.annotation.Controller;
import org.smart4j.chapter1.bean.Data;
import org.smart4j.chapter1.bean.Param;
import org.smart4j.chapter1.bean.View;
import org.smart4j.chapter1.model.dituQiu;
import org.smart4j.chapter1.model.shushuwu_content;
import org.smart4j.chapter1.model.zufang_dict;
import org.smart4j.chapter1.model.zufang_mast;
import org.smart4j.chapter1.util.DatabaseHelper;
import org.smart4j.chapter1.util.PhotoUtil;

import java.io.IOException;
import java.util.Date;
import java.util.List;

@Controller
public class zufangController {
    /**
     * 进入客户界面
     */
    @Action("get:/ditu")
    public View index(Param param) {

        return new View("ditu.html");
    }
    /**
     * 进入客户界面
     */
    @Action("get:/mast")
    public View mast(Param param) {

        return new View("mast.html");
    }
    /**
     * 进入客户界面
     */
    @Action("get:/")
    public View mast2(Param param) {

        return new View("mast2.html");
    }
    /**
     * 进入客户界面
     */
    @Action("get:/range")
    public View range(Param param) {

        return new View("range.html");
    }
    /**
     * 获取小区名列表
     */
    @Action("get:/getXiaoqu")
    public Data xiaoqu(Param param) {
        //String word = (String) param.getParamMap().get("word");

        String sql = "select xiaoqu from zufang_dict";
        List<zufang_dict> list = DatabaseHelper.queryEntityList(
                zufang_dict.class, sql);


        return new Data(list);
    }
    /**
     * 获取小区位置列表
     */
    @Action("get:/getDict")
    public Data site(Param param) {
        //String word = (String) param.getParamMap().get("word");

        String sql = "select * from zufang_dict";
        List<zufang_dict> list = DatabaseHelper.queryEntityList(
                zufang_dict.class, sql);
        return new Data(list);
    }

    /**
     * 获取小区图表
     */
    @Action("get:/getChart")
    public Data getChart(Param param) throws IOException {
        String xiaoqu = (String) param.getParamMap().get("xiaoqu");

        String sql = "select xiaoqu,room_area,money,unit_price,anxuan from zufang_mast where xiaoqu=?";
        List<zufang_mast> list = DatabaseHelper.queryEntityList(
                zufang_mast.class, sql,xiaoqu);
        if (list==null || list.size()<=0){
            System.out.println(xiaoqu);
        }
        String s=PhotoUtil.setChart(list);

        return new Data(s);
    }

    /**
     * 进入客户界面
     */
    @Action("post:/resultTime")
    public Data resultTime(Param param) {
        String word = (String) param.getParamMap().get("word");

        String sql = "select xiaoqu from zufang_dict";
        List<zufang_dict> list = DatabaseHelper.queryEntityList(
                zufang_dict.class, sql);


        return new Data(list);
    }
}