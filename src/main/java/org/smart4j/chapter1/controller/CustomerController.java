package org.smart4j.chapter1.controller;

import org.smart4j.chapter1.annotation.Action;
import org.smart4j.chapter1.annotation.Controller;
import org.smart4j.chapter1.annotation.Inject;
import org.smart4j.chapter1.bean.Param;
import org.smart4j.chapter1.bean.View;
import org.smart4j.chapter1.model.Customer;
import org.smart4j.chapter1.service.CustomerService;

import java.util.List;

@Controller
public class CustomerController {
    @Inject
    public CustomerService customerService;
    /**
     * 进入客户界面
     */
    @Action("get:/customer")
    public View index(Param param){
        List<Customer> customerList=customerService.getCustomerList();
        return new View("").addModel("customerList",customerList);
    }

}
