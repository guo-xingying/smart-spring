package org.smart4j.chapter1.controller;

import org.smart4j.chapter1.annotation.Action;
import org.smart4j.chapter1.annotation.Controller;
import org.smart4j.chapter1.bean.Param;
import org.smart4j.chapter1.bean.View;


@Controller
public class StaticController {
    /**
     * 进入客户界面
     */
    @Action("get:/jquery.js")
    public View index(Param param) {

        return new View("jquery.js");
    }
}