package org.smart4j.chapter1.model;

public class zufang_mast {
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShangquan() {
        return shangquan;
    }

    public void setShangquan(String shangquan) {
        this.shangquan = shangquan;
    }

    public String getXiaoqu() {
        return xiaoqu;
    }

    public void setXiaoqu(String xiaoqu) {
        this.xiaoqu = xiaoqu;
    }

    public String getRoom_type() {
        return room_type;
    }

    public void setRoom_type(String room_type) {
        this.room_type = room_type;
    }

    public int getRoom_area() {
        return room_area;
    }

    public void setRoom_area(int room_area) {
        this.room_area = room_area;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getJingjiren() {
        return jingjiren;
    }

    public void setJingjiren(String jingjiren) {
        this.jingjiren = jingjiren;
    }

    public String getJingjigongsi() {
        return jingjigongsi;
    }

    public void setJingjigongsi(String jingjigongsi) {
        this.jingjigongsi = jingjigongsi;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public int getAnxuan() {
        return anxuan;
    }

    public void setAnxuan(int anxuan) {
        this.anxuan = anxuan;
    }

    private String        url;
    private String title;
    private String        shangquan;;
    private String xiaoqu;
    private String        room_type;
    private  int room_area;
    private String        address;
    private String jingjiren;
     private String       jingjigongsi;
    private int money;
    private int        anxuan;
}
