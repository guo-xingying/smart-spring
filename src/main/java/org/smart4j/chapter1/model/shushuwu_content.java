package org.smart4j.chapter1.model;

public class shushuwu_content {
    private long id;
    private String title;
    private String author;
    private int chapter01;
    private int chapter02;
    private int chapter02_xiu;
    private int chapter03;
    private String content;
    private String content_xiu;
    private String url;
    private String meta;

    public String getMeta() {
        return meta;
    }

    public void setMeta(String meta) {
        this.meta = meta;
    }




    public int getChapter02_xiu() {
        return chapter02_xiu;
    }

    public void setChapter02_xiu(int chapter02_xiu) {
        this.chapter02_xiu = chapter02_xiu;
    }

    public int getChapter03() {
        return chapter03;
    }

    public void setChapter03(int chapter03) {
        this.chapter03 = chapter03;
    }
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getChapter01() {
        return chapter01;
    }

    public void setChapter01(int chapter01) {
        this.chapter01 = chapter01;
    }

    public int getChapter02() {
        return chapter02;
    }

    public void setChapter02(int chapter02) {
        this.chapter02 = chapter02;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContent_xiu() {
        return content_xiu;
    }

    public void setContent_xiu(String content_xiu) {
        this.content_xiu = content_xiu;
    }
}
