package org.smart4j.chapter1.model;

public class zufang_dict {
    private int id;
    private String xiaoqu;


    private double weidu;
    private double jingdu;
    private double unit_price_med;
    private double money_med;
    private int count;

    public double getWeidu() {
        return weidu;
    }

    public void setWeidu(double weidu) {
        this.weidu = weidu;
    }

    public double getJingdu() {
        return jingdu;
    }

    public void setJingdu(double jingdu) {
        this.jingdu = jingdu;
    }

    public double getUnit_price_med() {
        return unit_price_med;
    }

    public void setUnit_price_med(double unit_price_med) {
        this.unit_price_med = unit_price_med;
    }

    public double getMoney_med() {
        return money_med;
    }

    public void setMoney_med(double money_med) {
        this.money_med = money_med;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getXiaoqu() {
        return xiaoqu;
    }

    public void setXiaoqu(String xiaoqu) {
        this.xiaoqu = xiaoqu;
    }





}
