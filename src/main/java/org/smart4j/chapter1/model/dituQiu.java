package org.smart4j.chapter1.model;

import org.smart4j.chapter1.bean.Data;

public class dituQiu {
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    private int id;
    private String xiaoqu;
    private double weidu;
    private double jingdu;
    private int coun;

    public String getXiaoqu() {
        return xiaoqu;
    }

    public void setXiaoqu(String xiaoqu) {
        this.xiaoqu = xiaoqu;
    }

    public double getWeidu() {
        return weidu;
    }

    public void setWeidu(double weidu) {
        this.weidu = weidu;
    }

    public double getJingdu() {
        return jingdu;
    }

    public void setJingdu(double jingdu) {
        this.jingdu = jingdu;
    }

    public int getCoun() {
        return coun;
    }

    public void setCoun(int coun) {
        this.coun = coun;
    }
}
