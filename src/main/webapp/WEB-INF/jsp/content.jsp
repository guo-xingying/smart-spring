<%--
  Created by IntelliJ IDEA.
  User: guo_101
  Date: 2021/3/5
  Time: 21:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="BASE" value="${pageContext.request.contextPath}"/>
<c:set var="shu" value="${shushuwuList}"/>
<html>
<head>
    <title>正文</title>
</head>
<body>

<table>
    <c:forEach var="shushuwu" items="${shushuwuList}">
        <span style="white-space: pre-line">${shushuwu.content_xiu}</span>

    </c:forEach>
</table>

</body>
</html>
