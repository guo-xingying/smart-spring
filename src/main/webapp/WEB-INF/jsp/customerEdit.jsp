<%--
  Created by IntelliJ IDEA.
  User: guo_101
  Date: 2021/3/5
  Time: 21:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="BASE" value="${pageContext.request.contextPath}"/>
<html>
<head>
    <title>客户管理</title>
</head>
<body>
<h1>客户列表</h1>
<table>
    <tr>
        <th>客户名称</th>
        <th>联系人</th>
        <th>电话号码</th>
        <th>邮箱</th>
        <th>操作</th>
    </tr>
        <tr>
            <th>${customer.name}</th>
            <th>${customer.chapter01}</th>
            <th>${customer.content_xiu}</th>
            <th>
                <a href="${BASE}/customer_edit?id=${customer.id}">编辑</a>
            </th>
        </tr>

</table>

</body>
</html>
