


function getTransfer(LngLat_origin,data) {

    for (j = 0, len = data.length; j < len; j++) {
        (function (j) {

            transfer.search(LngLat_origin, new AMap.LngLat(data[j].jingdu, data[j].weidu), function (status, result) {
                if (status === 'complete') {
                    if (result.info == 'OK') {
                        //console.log(data[j].xiaoqu + "," + result.plans[0].time);
                        data[j][time]=result.plans[0].time;
                    } else if (result.info == 'NO_DATA') {
                        //console.log(data[j].xiaoqu + "," + "0");
                        data[j][time]=0;
                    }
                } else {
                    console.log(data[j].xiaoqu + "," + "公交路线数据查询失败")
                }
            });

        }(j));
    }
}

/**
 * 显示全部
 *      颜色
 * 显示安选
 *      颜色
 * 显示通勤范围
 *      颜色
 * @param color
 * @param anxuan
 */
function draw_xiaoqu(color,anxuan,unit_price_ned) {
    var vars=new Array(5000);
    if(!color){
        var Color='rgba(0,0,255,1)';
    }else{
        var Color=getColor(parseInt(data_locate[x].unit_price_med/4));
    }
    for(j = 0,len=data_locate.length; j < len; j++) {
        (function(x) {
            if (anxuan==0 || (anxuan==1 && data_locate[x].anxuan==1)){
                if (unit_price_ned>data_locate[x].unit_price_ned){
                    var center=[data_locate[x].jingdu,data_locate[x].weidu];
                    vars[x]= new AMap.CircleMarker({
                        center:center,
                        radius:getRadius(data_locate[x].count),//3D视图下，CircleMarker半径不要超过64px
                        strokeColor:'white',
                        strokeWeight:2,
                        strokeOpacity:0.5,
                        fillColor:Color,
                        fillOpacity:0.5,
                        zIndex:10,
                        bubble:true,
                        cursor:'pointer',
                        clickable: true
                    })
                    clickListener = AMap.event.addListener(vars[x], "click", function(e) {
                        $("#tip").text("小区:"+data_locate[x].xiaoqu);
                        $.get("./getChart?xiaoqu="+data_locate[x].xiaoqu,function(data,status){
                            //alert("数据：" + data + "\n状态：" + status);
                            $('#chart').attr('src',data);
                        });
                    })
                    vars[x].setMap(map)
                }

            }
        })(j)

    }

}
function getColor(val){
    var one=parseInt(255+255)/60;
    var r=0,g=0,b=0;
    if (val < 30)//第一个三等分
    {
        r = parseInt(one * val);
        g = 255;
    }
    else if (val >= 30 && val < 60)//第二个三等分
    {
        r = 255;
        g = 255 - parseInt((val - 30) * one);//val减最大取值的三分之一
    }
    else { r = 255;}
    return `rgb(${r},${g},${b})`;
}
function getRadius(val) {
    if(val==0){
        return 0;
    }else if (val<45){
        return parseInt(3+val/15);
    }else if (val<120){
        return parseInt(6+val/30)
    }else{
        return parseInt(10+val/100)
    }
}
//不同通勤时间显示不同颜色
function markerColorTime(data) {
    for(var i=0;i<data.length;i++){
        if (data[i].jingdu!=null && data[i].jingdu!=0){
            var bus_t=data[i].bus_time;
            if (1<=bus_t && bus_t<1800){
                marker_bus([data[i].jingdu,data[i].weidu], 'rgba(255,0,0,1)',data[i].xiaoqu);
            }else if (1800<=bus_t && bus_t<2400){
                marker_bus([data[i].jingdu,data[i].weidu], 'rgba(255,20,147,1)',data[i].xiaoqu);
            }else if (2400<=bus_t && bus_t<3000){
                marker_bus([data[i].jingdu,data[i].weidu], 'rgba(255,165,0,1)',data[i].xiaoqu);
            }else if (3000<=bus_t && bus_t<3600){
                marker_bus([data[i].jingdu,data[i].weidu], 'rgba(210,105,30,1)',data[i].xiaoqu);
            }else if (3600<=bus_t && bus_t<4200){
                marker_bus([data[i].jingdu,data[i].weidu], 'rgba(0,128,0,1)',data[i].xiaoqu);
            }else if (4200<=bus_t && bus_t<4800){
                marker_bus([data[i].jingdu,data[i].weidu], 'rgba(0,0,255,1)',data[i].xiaoqu);
            }
        }else {
            var aasdasd=1;
        }
    }
}
//不同通勤时间显示不同颜色
function marker_bus(center,color,xiaoqu) {
    var mm1= new AMap.CircleMarker({
        center:center,
        radius:7,//3D视图下，CircleMarker半径不要超过64px
        strokeColor:'white',
        strokeWeight:2,
        strokeOpacity:0.5,
        fillColor:color,
        fillOpacity:0.5,
        zIndex:10,
        bubble:true,
        cursor:'pointer',
        clickable: true
    })
    clickListener = AMap.event.addListener(mm1, "click", function(e) {
        //$("#tip").text("小区:"+data[x].xiaoqu);
        $.get("./getChart?xiaoqu="+xiaoqu,function(data,status){
            //alert("数据：" + data + "\n状态：" + status);
            $('#chart').attr('src',data);
        });
    })
    mm1.setMap(map)

}