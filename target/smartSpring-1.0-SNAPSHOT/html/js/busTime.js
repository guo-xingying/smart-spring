function busTime(data,address){
    var date_temp=[];
    for (var i=0;i<data.length;i++){
        var distance=getDisance(data[i].weidu,data[i].jingdu,address[1],address[0]);
        if (distance<15000){
            date_temp.push(data[i]);
        }
    }
    var res_time=[]
    var transOptions = {
        city: '北京市',
        //设置公交换乘策略
        policy: AMap.TransferPolicy.LEAST_TIME
    };
    //构造公交换乘类
    var transfer = new AMap.Transfer(transOptions);
    for(k = 0,len=date_temp.length; k < 200; k++) {
        (function (j) {

            transfer.search(new AMap.LngLat(address[0],address[1]), new AMap.LngLat(date_temp[j].jingdu, date_temp[j].weidu), function(status, result) {
                if (status === 'complete') {
                    if(result.info=='OK'){
                        res_time.push({xiaoqu_id:date_temp[j].id,bus_time:result.plans[0].time});
                        date_temp[j].bus_time=result.plans[0].time;
                        //console.log(date_temp[j].id+","+result.plans[0].time);
                    }else if(result.info=='NO_DATA'){
                        res_time.push({xiaoqu_id:date_temp[j].id,bus_time:0});
                        date_temp[j].bus_time=0;
                    }
                } else {
                    console.log(date_temp[j].xiaoqu+","+"公交路线数据查询失败")
                }
                if (j==200-1){
                    markerColorTime(date_temp);
                }
            });
        }(k));
    }
/*    $.post("setBusTime",res_time,function(data,status){
        alert("数据：" + data + "\n状态：" + status);
    });*/
    //return date_temp;
}


function toRad(d) {  return d * Math.PI / 180; }
function getDisance(lat1, lng1, lat2, lng2) {
    //lat为纬度, lng为经度, 一定不要弄错

    var dis = 0;

    var radLat1 = toRad(lat1);

    var radLat2 = toRad(lat2);

    var deltaLat = radLat1 - radLat2;

    var deltaLng = toRad(lng1) - toRad(lng2);

    var dis = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(deltaLat / 2), 2) + Math.cos(radLat1) * Math.cos(radLat2) * Math.pow(Math.sin(deltaLng / 2), 2)));

    return dis * 6378137;

}